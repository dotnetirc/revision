# Contrôle ASP.Net

- Créer une application client 
- Faire une couche model (sans FK)
- Ajouter de champs obligatoires (Required)
- Définir des valeur par defaut
- Définir des NB de decimal
- Faire de la BDD CodeFirst 
- Faire une api Asynchrone  

---  
8/20 

---

- Faire une recherche (Cf TP3 - Recherche par mail)
- Documenter l'api
- Faire des test unitaires
- Mettre en place le pattern repository (TP3 - Partie 2)
- Faire une App client (UWP)
- Utiliser le webService (Copier-Coller le WSService - TP1)
- Recherche et création d'entity (Musique - l'an passé)
- Point Bonus pour qualité de code / commentaires


# Model BDD

||Musique|Artiste||
|:---:| :---: | :---: |:---:|
|Obligatoire|id_musique|id_artiste|Obligatoire|
|Obligatoire|libelle_musique|libelle_artiste|Obligatoire|
|3 décimal max|duree_musique|annee_naissance_artiste||
|défaut : 1900|annee_musique|||
|NULL|Artiste|||


# Consigne

Créer une application client et une api asynchrone documentée qui permettent la recherche et la creation de musiques. Vous ajouter des champs obligatoires ainsi que des valeur par defaut comme indiqué sur le model. Ce projet se réalise selon la logique code first en appliquant le pattern repository et comportera des test unitaires. Vous penserez à utiliser le webService.  
La qualité de code sera prise en compte

<!-- appli
couche model (sasan fk)
required
val par defaut
codefirst
api asynchrone
-------------8-------------------
recherche cf TP 3 
Documenter l'Api
test unitaire
pattern repository
App client (uwp)
Copier coller WSService (TP1)
recherche et creation 
point bonus si commentaire -->

