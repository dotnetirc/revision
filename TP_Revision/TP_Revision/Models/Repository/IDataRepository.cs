﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TP_Revision.Models.EntityFramework;

namespace TP_Revision.Models.Repository
{
    public interface IDataRepository<TEntity>
    {
        Task<ActionResult<IEnumerable<Musique>>> GetAll();
        Task<ActionResult<Musique>> GetById(int id);
        Task<ActionResult<IEnumerable<TEntity>>> GetByString(string str);
        Task Add(TEntity entity);
        Task Update(TEntity entityToUpdate, TEntity entity);
        Task Delete(TEntity entity);
    }
}
