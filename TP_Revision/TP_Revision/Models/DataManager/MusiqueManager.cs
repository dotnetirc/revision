﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TP_Revision.Models.EntityFramework;
using TP_Revision.Models.Repository;

namespace TP_Revision.Models.DataManager
{
    public class MusiqueManager : IDataRepository<Musique>
    {
        readonly TpRevisionContext tpRevisionContext;

        public async Task Add(Musique entity)
        {
            tpRevisionContext.Musique.Add(entity);
            await tpRevisionContext.SaveChangesAsync();
        }

        public async Task Delete(Musique entity)
        {
            tpRevisionContext.Musique.Remove(entity);
            await tpRevisionContext.SaveChangesAsync();
        }

        public async Task<ActionResult<IEnumerable<Musique>>> GetAll()
        {
            return await tpRevisionContext.Musique.ToListAsync();
        }

        public async Task<ActionResult<Musique>> GetById(int id)
        {
            return await tpRevisionContext.Musique.FirstOrDefaultAsync(e => e.MusiqueId == id);
        }

        public async Task<ActionResult<IEnumerable<Musique>>> GetByString(string str)
        {
            var musiques = from m in tpRevisionContext.Musique
                select m;

            if (!String.IsNullOrEmpty(str))
            {
                musiques = musiques.Where(s => s.Libelle.ToUpper() == str.ToUpper());
            }

            return await musiques.ToListAsync();
        }

        public async Task Update(Musique entityToUpdate, Musique entity)
        {
            tpRevisionContext.Entry(entityToUpdate).State = EntityState.Modified;
            entityToUpdate.MusiqueId = entity.MusiqueId;
            entityToUpdate.Annee = entity.Annee;
            entityToUpdate.Artiste = entity.Artiste;
            entityToUpdate.Duree = entity.Duree;
            entityToUpdate.Libelle = entity.Libelle;

            await tpRevisionContext.SaveChangesAsync();

        }

        public MusiqueManager(TpRevisionContext context)
        {
            tpRevisionContext = context;
        }
    }
}
