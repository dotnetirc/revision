﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TP_Revision.Models.EntityFramework
{
    public class TpRevisionContext : DbContext
    {
        /// <summary>
        /// COnstructor
        /// </summary>
        public TpRevisionContext()
        {}

        public TpRevisionContext(DbContextOptions<TpRevisionContext> options) : base(options)
        {
        }

        /// <summary>
        /// Connexion avec l'entité
        /// </summary>
        public virtual DbSet<Musique> Musique { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");

            modelBuilder.Entity<Musique>(entity =>
            {
                /// Primery Key
                entity.HasKey(e => new { e.MusiqueId }).HasName("PK_MUSIQUE");
                /// Contraint d'unicité
                entity.HasIndex(e => e.Libelle)
                    .HasName("UQ_MUSIQUE_LIBELLE")
                    .IsUnique();

                entity.Property(e => e.Annee).HasDefaultValue(1900);
            });

        }
    }
}
