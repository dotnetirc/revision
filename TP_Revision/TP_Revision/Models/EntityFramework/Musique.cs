﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TP_Revision.Models.EntityFramework
{
    [Table("T_E_MUSQIUE_MUSIQUE")]
    public class Musique
    {
        /// <summary>
        /// Attributs de la musique
        /// </summary>
        [Required]
        [Column("MUSIQUE_ID")]
        [Key]
        public int MusiqueId { get; set; }

        /// <summary>
        /// Obligatoire
        /// Nom de colonne :  LIBELLE_MUSIQUE
        /// Nom affiché : Libellé Musique
        /// Longueur maximal : 100 caractères
        /// </summary>
        [Required]
        [Column("LIBELLE_MUSIQUE")]
        [Display(Name = "Libellé Musique")]
        [StringLength(100)]
        public string Libelle { get; set; }

        /// <summary>
        ///  ? : Peut être null (uniquement pour les nombres)
        ///  précision (3,0)
        /// </summary>
        [Column("DUREE_MUSIQUE", TypeName = "numeric(3,0)")]
        public decimal? Duree { get; set; }

        [Column("ANNEE_MUSIQUE",TypeName = "char(4)")]
        public string Annee { get; set; }

        [Column("ARTISTE_MUSIQUE")]
        public string Artiste { get; set; }

    }
}
