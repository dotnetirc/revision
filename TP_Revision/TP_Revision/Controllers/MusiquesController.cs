﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TP_Revision.Models.EntityFramework;
using TP_Revision.Models.Repository;

namespace TP_Revision.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MusiquesController : ControllerBase
    {
        private readonly IDataRepository<Musique> dataRepository;

        public MusiquesController(IDataRepository<Musique> dataRepo)
        {
            dataRepository = dataRepo;
        }

        // GET: api/Musiques
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Musique>>> GetMusique(string musique)
        {          
            return await dataRepository.GetByString(musique);
        }

        // GET: api/Musiques/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Musique>> GetMusiqueById(int id)
        {
            var musique = await dataRepository.GetById(id);

            if (musique == null)
            {
                return NotFound();
            }

            return musique;
        }

        // PUT: api/Musiques/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMusique(int id, Musique musique)
        {
            if (id != musique.MusiqueId)
            {
                return BadRequest();
            }
            var musiqueToUpdate = await dataRepository.GetById(id);

            if(musiqueToUpdate == null)
            {
                return NotFound();
            }

            await dataRepository.Update(musiqueToUpdate.Value, musique);

            return NoContent();
        }

        // POST: api/Musiques
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Musique>> PostMusique(Musique musique)
        {
            await dataRepository.Add(musique);

            return CreatedAtAction("GetMusiqueById", new { id = musique.MusiqueId }, musique);
        }

        // DELETE: api/Musiques/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Musique>> DeleteMusique(int id)
        {
            var musique = await dataRepository.GetById(id);
            if (musique == null)
            {
                return NotFound();
            }

            await dataRepository.Delete(musique.Value);

            return musique;
        }
    }
}
