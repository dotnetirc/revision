﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace TP_Revision.Migrations
{
    public partial class creation_db : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "T_E_MUSQIUE_MUSIQUE",
                schema: "public",
                columns: table => new
                {
                    MUSIQUE_ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    LIBELLE_MUSIQUE = table.Column<string>(maxLength: 100, nullable: false),
                    DUREE_MUSIQUE = table.Column<decimal>(type: "numeric(3,0)", nullable: true),
                    ANNEE_MUSIQUE = table.Column<string>(type: "char(4)", nullable: true, defaultValue: "1900"),
                    ARTISTE_MUSIQUE = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MUSIQUE", x => x.MUSIQUE_ID);
                });

            migrationBuilder.CreateIndex(
                name: "UQ_MUSIQUE_LIBELLE",
                schema: "public",
                table: "T_E_MUSQIUE_MUSIQUE",
                column: "LIBELLE_MUSIQUE",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "T_E_MUSQIUE_MUSIQUE",
                schema: "public");
        }
    }
}
