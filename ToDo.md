# Fiche de réalisation

- Creation de projet

![](./images/1.png)

- Installer le spackage Nugets
    - Microsoft.EntityFrameworkCore.Tools
    - Npgsql.EntityFrameworkCore.PostgreSQL

- Créer un dossier `Models`
- Créer un sous-dossier `EntityFramework`
- Créer les classes du model (entity)
    - Mettre les annotations
- Créer la classe `Context`
    - Définir les valeur par defaut et contraintes (pk, fk)
- Déclaration de la BDD
    - appsettings.json -> rajouter string de connexion `ConnectionStrings": {
    "TP3Context": "Server=localhost;port=5432;Database=DBName; uid=postgres; password=password;"
  }`
- Compliler  
![](./images/2.png)
- Faire les migrations 
    - `dotnet tool install --global dotnet-ef`
    - Check version `dotnet ef`
    - Ajouter à `Startup.cs` dans `ConfigureServices`: 

            services.AddDbContext<TpRevisionContext>(options => options.UseNpgsql(Configuration.GetConnectionString("TpRevisionContext")));
    - run `dotnet-ef migrations add "creation_db" --project "TP_Revision"` 
    -   update DB  : `dotnet-ef datab0se update –-project "TP_Revision"`
        - si lvl `.sln` add `–-project "TP_Revision"`
    - Générer le controller  (Dossier controller > Ajouter > Controller)  

![](./images/3.png)  
![](./images/4.png)
- Mise en place du pattern repository pour les recherche
    - Creation dossier Models>Repository & Models>
    - Repository > IDataRepository.cs (interface public)
    - DataManager > MusiqueManager.cs
    - Ajouter un constructeur 
- Ajouter nuGet `Swashbuckle.AspNetCore`
- Modifier startus.cs
- Ajouter la proprieté sur le projets (clique droit > properties)  
![](./images/5.png) 
- access `https://localhost:44310/swagger/index.html`

## test unitaire
- Ajouter une solution  
![](./images/6.png) 
- Ajouter une reference au projet `clique droit > ajouter > réference`
- Installer NuGet Microsoft.EntityFrameworkCore.Tools sur le projet test


## Application client
- Créer un nouveau projet
![](./images/7.png)
- Ajouter le NuGet `MVVMLight` & `WebApi.Client`
- Créer dossiers `Model` & `Service` & `ViewModel`
    - Créer model> Musique.cs (copier l'api- tous le model de l'api)
    - Service > WsService.cs (copier de TP1)
- Ajouter ViewModel> `MainViewModel.cs`
- Créer un dossier `View`
- Deplacer `MainPage.xaml` dans le dossier `View`
- Designer la page